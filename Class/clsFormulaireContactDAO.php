<?php

require_once 'clsEncrypt.php';

require_once 'clsFormulaireContact.php';

class FormulaireContactDAO{
  
  public static function getFormHTML(){
    $html = '';
    $html .= '<h1>Écrivez-nous</h1>';

    $html .= '<div class="form-group row">';
    $html .= '<div class="col-md-6">
                <h4>Paroisse Saint-Georges-de-Sartigan</h4>
                <ul>
                 <li>Assomption de la BVM </li>
                 <li>Notre-Dame-de-la-Providence</li>
                 <li>Sainte-Aurélie</li>
                 <li>Saint-Benjamin</li>
                 <li>Saint-Côme</li>
                 <li>Saint-Georges</li>
                 <li>Saint-Jean-de-la-Lande</li>
                 <li>Saint-Philibert</li>
                 <li>Saint-Prosper</li>
                 <li>Saint-René-Goupil</li>
                 <li>Saint-Simon</li>
                 <li>Saint-Zacharie</li>
                </ul>
                <p>
                  1890, 1re Avenue Ouest<br />Saint-Georges (QC) G5Y 3N5<br />
                  Téléphone : 418-228-2558<br />
                  Fax : 418-228-4932<br />
                  Courriel : fabstgeorges@globetrotter.net
                </p>
              </div>';
    $html .= '<div class="col-md-6">
              <h4>Saint-Jean-Paul II </h4>
              <ul>
                <li>Saint-Gédéon</li>
                <li>Saint-Ludger</li>
                <li>Saint-Martin</li>
                <li>Saint-Robert-Bellarmin</li>
                <li>Saint-Théophile</li>
              </ul>
             <p>
                101 1re Avenue Sud<br />Saint-Gédéon (QC) G0M 1T0<br />
                Téléphone : 418-582-3434<br />
                Courriel : fabriquestgedeon@cgocable.ca
             </p>
            </div>';
    $html .= '</div>';


    $html .= '<form class="container col-md-6" id="formConnexion">
      <div class="form-group row">
        <div class="col-md-6">
          <label for="ffirstname" class=" col-form-label">Votre prénom</label>
  
          <input type="text" class="form-control" id="ffirstname" name="ffirstname" maxlength="100" tabindex="10" placeholder="Prénom">
        </div>
        <div class="col-md-6">
          <label for="flastname" class=" col-form-label">Votre nom</label>
          <input type="text" class="form-control" id="flastname" name="flastname" maxlength="100" tabindex="20" placeholder="Nom">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-md-6">
          <label for="fcourriel" class=" col-form-label">Votre adresse courriel</label>
          <input type="text" class="form-control" id="fcourriel" name="fcourriel" maxlength="100" tabindex="30" placeholder="Courriel">
        </div>
        <div class="col-md-6">
          <label for="ftelephone" class="col-form-label">Votre numéro de téléphone</label>
          <input type="text" class="form-control" id="ftelephone" name="ftelephone" maxlength="100" tabindex="40" placeholder="Numéro de téléphone">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-md-12">
          <label for="fmessage" class="col-md-6 col-form-label">Votre message</label>
          <textarea class="form-control" id="fmessage" tabindex="50" name="fmessage" maxlength="8000" placeholder="Votre message" tabindex="50"></textarea>
        </div>
      </div>
    </form>
    <br>
    <div class="Center">
      <input class="btn btn-primary" type="button" tabindex="60" name="btnSubmitForm" value="Envoyer" onclick="fnSubmit();">
    </div><br />
    <div class="form-group row">
        <div class="col-md-12">&nbsp;</div>
    </div>';

    echo $html;
  }

  public static function saveFormulaire($prenom, $nom, $courriel, $telephone, $message){
    
    if (FormulaireContactDAO::validateFormulaire($prenom, $nom, $courriel, $telephone, $message)){
      $encrypt = new Encryption();

      $key = $encrypt->generateKey();
      $iv  = $encrypt->generateIV();

      $conn = OpenCon();

      $prenom = $encrypt->encryptData($prenom, $key, $iv);
      $nom    = $encrypt->encryptData($nom, $key, $iv);
      $courriel = $encrypt->encryptData($courriel, $key, $iv);
      $telephone = $encrypt->encryptData($telephone,$key, $iv);

      $param = "'".$conn->real_escape_string($prenom)."','".$conn->real_escape_string($nom)."','".$conn->real_escape_string($courriel)."','".$conn->real_escape_string($telephone)."','".$conn->real_escape_string($message)."','".$conn->real_escape_string($key)."','".$conn->real_escape_string($iv)."'";

      $SQL = 'CALL FormulaireContactSave('.$param.');';
      
      if (!$conn->query($SQL)){
        echo $SQL;
        exit('fail');
      }

      CloseCon($conn);

      exit('success');
    }else {
      exit('emptyFields');
    }
  }

  public static function validateFormulaire($prenom, $nom, $courriel, $telephone, $message){
    if (TRIM($prenom) == '' OR TRIM($nom) == '' OR TRIM($courriel) == '' OR TRIM($telephone) == '' OR TRIM($message) == ''){
      return false;
    }
    return true;
  }

  public static function getFormList(){
    $conn = OpenCon();

    $SQL  = 'CALL GetAllFormContact();';

    if (!$RSSQL = $conn->query($SQL)){
      exit('Une erreur est survenu dans la requête à la base de données.');
    }

    CloseCon($conn);

    $formlist = array();

    if ($RSSQL->num_rows > 0){
      while ($row = $RSSQL->fetch_assoc()){
        array_push($formlist, new FormulaireContact($row['formulaireid'], $row['prenom'], $row['nom'], $row['courriel'], $row['telephone'],
                   $row['message'], $row['key'], $row['iv']));
      }
      return $formlist;
    }
  }

  public static function getFormSpecData($formid){
    $conn = OpenCon();

    $SQL  = "CALL GetFormContact('".$formid."');";

    if (!$RSSQL = $conn->query($SQL)){
      exit('Une erreur est survenu dans la requête à la base de données.');
    }

    CloseCon($conn);

    if ($RSSQL->num_rows > 0){
      $row = $RSSQL->fetch_assoc();
      $formdata = new FormulaireContact($row['formulaireid'], $row['prenom'], $row['nom'], $row['courriel'], $row['telephone'],
                                        $row['message'], $row['key'], $row['iv']);
      $formdata->getFormDataDetails();
    }
  }

  public static function deleteForm($formid){
    $SQL = "CALL DeleteFormContact('".$formid."')";

    $conn = OpenCon();

    if (!$conn->query($SQL)){
      return 'fail';
    }

    CloseCon($conn);

    return 'success';
  }

}


?>