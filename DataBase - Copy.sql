
DELIMITER ;;
CREATE PROCEDURE `DeleteEvent`(IN inEventId INT(11))
BEGIN
	DELETE FROM calendarevent WHERE eventId = inEventId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteFeuillet`(IN inFeuilletId INT(11))
BEGIN
	DELETE FROM feuillet WHERE FeuilletId = inFeuilletId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteFormBenevolat`(IN inFormId INT(11))
BEGIN
	DELETE FROM formulairebenevolat 
    WHERE formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteFormContact`(IN inFormId INT(11))
BEGIN
	DELETE FROM formulairecontact 
    WHERE  formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteFormEnfant`(IN inFormId INT(11))
BEGIN
	DELETE FROM formulaireenfant
    WHERE formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteImageMagasin`(IN inImageMagasinId INT(11))
BEGIN
	DELETE FROM imagemagasin WHERE imageMagasinId = inImageMagasinId;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE PROCEDURE `DeleteNouvelles`(
	in nouvelleId INT(11))
BEGIN
DELETE FROM `nouvelles` WHERE nouvellesId = nouvelleId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `FormulaireBenevolatSave`(IN inEmail VARBINARY(1000), IN inFirstName VARBINARY(1000), IN inLastName VARBINARY(1000), IN inAdresse VARBINARY(1000), IN inTelephone VARBINARY(1000),
																	  IN inCellulaire VARBINARY(1000), IN inBenevolat1 VARCHAR(500), IN inBenevolat2 VARCHAR(500), IN inBenevolat3 VARCHAR(500), IN inBenevolat4 VARCHAR(500),
                                                                      IN inParoisseId INT(11), IN inCommunauteId INT(11),IN inLundiAM TINYINT(4), IN inLundiPM TINYINT(4), IN inLundiSOIR TINYINT(4), IN inMardiAM TINYINT(4), IN inMardiPM TINYINT(4), IN inMardiSOIR TINYINT(4), IN inMercrediAM TINYINT(4), IN inMercrediPM TINYINT(4), IN inMercrediSOIR TINYINT(4),
																	  IN inJeudiAM TINYINT(4), IN inJeudiPM TINYINT(4), IN inJeudiSOIR TINYINT(4), IN inVendrediAM TINYINT(4), IN inVendrediPM TINYINT(4), IN inVendrediSOIR TINYINT(4), IN inSamediAM TINYINT(4), IN inSamediPM TINYINT(4), IN inSamediSOIR TINYINT(4),
                                                                      IN inDimancheAM TINYINT(4), IN inDimanchePM TINYINT(4), IN inDimancheSOIR TINYINT(4), IN inKey VARBINARY(1000), IN inIV VARBINARY(1000))
BEGIN
	INSERT INTO formulairebenevolat
	(`prenom`,
	`nom`,
	`adresse`,
	`telephone`,
	`cellulaire`,
	`courriel`,
	`benevolat1`,
	`benevolat2`,
	`benevolat3`,
	`benevolat4`,
	`paroisseid`,
    `communauteid`,
	`lundiam`,
	`lundipm`,
	`lundisoiree`,
	`mardiam`,
	`mardipm`,
	`mardisoiree`,
	`mercrediam`,
	`mercredipm`,
	`mercredisoiree`,
	`jeudiam`,
	`jeudipm`,
	`jeudisoiree`,
	`vendrediam`,
	`vendredipm`,
	`vendredisoiree`,
	`samediam`,
	`samedipm`,
	`samedisoiree`,
	`dimancheam`,
	`dimanchepm`,
	`dimanchesoiree`,
    `key`,
    `iv`)
	VALUES
	(inFirstName,
	inLastName,
	inAdresse,
	inTelephone,
	inCellulaire,
	inEmail,
	inBenevolat1,
	inBenevolat2,
	inBenevolat3,
	inBenevolat4,
	inParoisseId,
    inCommunauteId,
	inLundiAM,
	inLundiPM,
	inLundiSOIR,
	inMardiAM,
	inMardiPM,
	inMardiSOIR,
	inMercrediAM,
	inMercrediPM,
	inMercrediSOIR,
	inJeudiAM,
	inJeudiPM,
	inJeudiSOIR,
	inVendrediAM,
	inVendrediPM,
	inVendrediSOIR,
	inSamediAM,
	inSamediPM,
	inSamediSOIR,
	inDimancheAM,
	inDimanchePM,
	inDimancheSOIR,
    inKey,
    inIV);

END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `FormulaireContactSave`(IN inPrenom VARBINARY(1000), IN inNom VARBINARY(1000), IN inCourriel VARBINARY(1000), IN inTelephone VARBINARY(1000), IN inMessage VARCHAR(8000), IN inKey VARBINARY(1000), IN inIv VARBINARY(1000))
BEGIN
	INSERT INTO formulairecontact (prenom, nom, courriel, telephone, message, formulairecontact.key, iv)
    VALUES (inPrenom, inNom, inCourriel, inTelephone, inMessage, inKey, inIv);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `FormulaireEnfantSave`(IN inName VARBINARY(1000), IN inAdresse VARBINARY(1000), IN inCodePostal VARBINARY(1000), IN inCourriel VARBINARY(1000), IN inDateNaissance VARBINARY(1000),
																	  IN inNomPere VARBINARY(1000), IN inTelPere VARBINARY(1000), IN inNomMere VARBINARY(1000), IN inTelMere VARBINARY(1000), IN inBapteme VARCHAR(500), IN inPardon VARCHAR(500), IN inEucharistie VARCHAR(500), IN inAllergies VARCHAR(500),
                                                                      IN inParoisseId INT(11), IN inCommunauteId INT(11),IN inInitiation TINYINT(4), IN inPtitePasto TINYINT(4), IN inAgnelets TINYINT(4), IN inPremierPardon TINYINT(4), IN inPremiereCommunion TINYINT(4), IN inConfirmation TINYINT(4), IN inBrebis TINYINT(4),
                                                                      IN inKey VARBINARY(1000), IN inIV VARBINARY(1000))
BEGIN
	INSERT INTO formulaireenfant
	(`nom`,
	`adresse`,
	`codepostal`,
	`courriel`,
	`datenaissance`,
	`nompere`,
	`telpere`,
	`nommere`,
	`telmere`,
	`bapteme`,
	`pardon`,
    `eucharistie`,
    `allergies`,
    `paroisseid`,
    `communauteid`,
	`initiation`,
	`ptitepasto`,
	`agnelets`,
	`premierpardon`,
	`premierecommunion`,
	`confirmation`,
	`brebis`,	
    `key`,
    `iv`)
	VALUES
	(inName,
	inAdresse,
	inCodePostal,
	inCourriel,
	inDateNaissance,
	inNomPere,
	inTelPere,
    inNomMere,
    inTelMere,
	inBapteme,
	inPardon,
	inEucharistie,
    inAllergies,
	inParoisseId,
    inCommunauteId,
	inInitiation,
	inPtitePasto,
	inAgnelets,
	inPremierPardon,
	inPremiereCommunion,
	inConfirmation,
	inBrebis,
    inKey,
    inIV);

END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAccueil`()
BEGIN
	select imageHomeliePath, imageTemoignagePath, videoBienvenuePath, imageFormulairePath from accueil order by accueilId desc limit 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllCommunauteOrderParoisse`()
BEGIN
	SELECT nom, paroisseid, communauteid FROM communaute ORDER BY paroisseid, nom, communauteid;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllEvents`(IN inCommunityId INT(11))
BEGIN
	SELECT eventId, name, descr, date, color, icon, communityId, global FROM calendarevent WHERE communityId = inCommunityId OR global = 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllFeuillet`()
BEGIN
	select feuilletId, pdfPath, pdfName, actif, orderDisplay, size from feuillet where actif = 1 order by orderDisplay;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllFeuilletEdit`()
BEGIN
	select feuilletId, pdfPath, pdfName, actif, orderDisplay, size from feuillet  order by  orderDisplay , Actif;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllFormBenevolat`()
BEGIN
	SELECT formulaireid, courriel, paroisse.nom as 'nomparoisse', formulairebenevolat.key, iv 
    FROM formulairebenevolat
    LEFT JOIN paroisse ON paroisse.paroisseid = formulairebenevolat.paroisseid
    ORDER BY formulaireid DESC;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllFormContact`()
BEGIN
	SELECT *
    FROM formulairecontact 
    ORDER BY formulaireid DESC;
END ;;
DELIMITER ;

CREATE PROCEDURE `GetAllFormEnfant`()
BEGIN
	SELECT communaute.nom as 'communauteid', formulaireid, formulaireenfant.nom, adresse, codepostal, courriel,
	   datenaissance, nompere, telpere, nommere, telmere, bapteme, pardon, eucharistie, allergies, formulaireenfant.paroisseid, 
       initiation, ptitepasto, agnelets, premierpardon, premierecommunion, confirmation, brebis, formulaireenfant.key, iv 
	FROM formulaireenfant
	LEFT JOIN communaute ON communaute.communauteid = formulaireenfant.communauteid
    ORDER BY formulaireid DESC;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllImageMagasin`()
BEGIN
	select imageMagasinId, menuId, imagePath, imageName, actif, orderDisplay from imagemagasin where actif = 1 order by orderDisplay;
      
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllImageMagasinEdit`()
BEGIN
	select imageMagasinId, menuId, imagePath, imageName, actif, orderDisplay from imagemagasin  order by  orderDisplay , actif;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllNouvelles`()
BEGIN
	SELECT  nouvellesId, title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath
    FROM nouvelles
    WHERE (dateDebut >= UNIX_TIMESTAMP(CURDATE()) AND dateFin <= UNIX_TIMESTAMP(CURDATE())) OR actif = 1
    ORDER BY actif DESC, dateDebut, dateFin;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetAllNouvellesEdit`()
BEGIN
	SELECT  nouvellesId, title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath
    FROM nouvelles
    ORDER BY actif DESC, dateDebut, dateFin;
END ;;
DELIMITER ;

CREATE PROCEDURE `GetAllParoisse`()
BEGIN
	SELECT nom, paroisseid FROM paroisse ORDER BY nom, paroisseid;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetCommunityID`(IN inMenuID INT(11))
BEGIN
	SELECT communauteid, nom FROM communaute WHERE menuid = inMenuID;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetCommunityName`(IN inCommunityId INT(11))
BEGIN
	SELECT nom 
    FROM communaute 
    WHERE communauteid = inCommunityId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetCommunitySchedule`(IN inCommunityId INT(11))
BEGIN
	SELECT scheduleid, schedule, communityid FROM communityschedule WHERE communityid = inCommunityId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetFeuillet`(in inFeuilletId int)
BEGIN
	select feuilletId, pdfPath, pdfName, actif, orderDisplay, size from feuillet where feuilletId = inFeuilletId;
    
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetFormBenevolat`(IN inFormId INT(11))
BEGIN
	SELECT * FROM formulairebenevolat WHERE formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetFormContact`(IN inFormId INT(11))
BEGIN
	SELECT * FROM formulairecontact WHERE formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetFormEnfant`(IN inFormId INT(11))
BEGIN
	SELECT communauteid, formulaireid, nom, adresse, codepostal, courriel,
	   datenaissance, nompere, telpere, nommere, telmere, bapteme, pardon, eucharistie, allergies, paroisseid, 
       initiation, ptitepasto, agnelets, premierpardon, premierecommunion, confirmation, brebis, formulaireenfant.key, iv 
	FROM formulaireenfant
    WHERE formulaireid = inFormId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetImageMagasin`(in inImageMagasinId int)
BEGIN	
    select imageMagasinId, menuId, imagePath, imageName, actif, orderDisplay from imagemagasin  where imageMagasinId = inImageMagasinId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetMenus`(IN inParentid INT(11))
BEGIN
  SELECT menu.menuId, menu.name, menu.redirectionPath FROM menu WHERE parentId = inParentid ORDER BY sequence, name;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetNouvelle`(
	in nouvelleId int(11))
BEGIN
	select nouvellesId, title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath from nouvelles where nouvellesId = nouvelleId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetNouvellesBandeau`()
BEGIN
DECLARE x INT DEFAULT 0;
DECLARE y INT DEFAULT 0;

set x = (select count(nouvellesId) from nouvelles where actif = 1);
set x = (if (x <= 5, x, 5));
set y = 5 - x;


(select nouvellesId, title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath
from nouvelles
where actif = 1 
limit x)
union
(select nouvellesId, title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath
from nouvelles
where actif = 0 and dateDebut >= UNIX_TIMESTAMP(CURDATE()) and dateFin <= UNIX_TIMESTAMP(CURDATE())  
limit y)
order by actif desc , dateDebut,dateFin;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `GetPageContentTemplateText`(IN inMenuId INT(11))
BEGIN
	SELECT pagecontenttemplatetextid, menuid, image, title, header, subtitle, content FROM pagecontenttemplatetext WHERE menuid = inMenuId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `SaveCommunitySchedule`(IN inScheduleId INT(11), IN inSchedule LONGTEXT, IN inCommunityId INT(11))
BEGIN
	INSERT INTO communityschedule(schedule, communityid) SELECT inSchedule, inCommunityId WHERE inScheduleId = 0;
    
    UPDATE communityschedule SET schedule = inSchedule WHERE scheduleid = inScheduleId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `SavePageContentTemplateText`(IN inContentId INT(11), IN inMenuId INT(11), IN inTitle LONGTEXT, IN inHeader LONGTEXT, IN inSubtitle LONGTEXT, IN inContent LONGTEXT)
BEGIN
	INSERT INTO pagecontenttemplatetext(menuid, title, header, subtitle, content) SELECT inMenuId, inTitle, inHeader, inSubtitle, inContent WHERE inContentId = 0;
    
    UPDATE pagecontenttemplatetext SET title = inTitle, header = inHeader, subtitle = inSubtitle, content = inContent WHERE menuid = inMenuId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateEvent`(IN inEventId INT(11), IN inName VARCHAR(100), IN inDate VARCHAR(100), IN inDescr VARCHAR(500), IN inColor VARCHAR(50), IN inIcon VARCHAR(50), IN inGlobal TINYINT)
BEGIN
	UPDATE calendarevent SET name= inName, descr = inDescr, date = inDate, color = inColor, icon = inIcon, global = inGlobal WHERE eventId = inEventId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateFeuillet`(in inFeuilletId int, in inActif tinyint, in inOrderDisplay int)
BEGIN
	update feuillet
    SET 
        actif 		 = inActif,
        orderDisplay = inOrderDisplay
        
	WHERE
     feuilletId= inFeuilletId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateimageAccueil`(IN inImageHomeliePath VARCHAR(200), IN inImageTemoignagePath VARCHAR(200), IN inVideoBienvenuePath VARCHAR(200), IN inImageFormulairePath VARCHAR(200))
BEGIN
	UPDATE accueil SET imageHomeliePath= inImageHomeliePath, imageTemoignagePath = inImageTemoignagePath, videoBienvenuePath = inVideoBienvenuePath, imageFormulairePath = inImageFormulairePath WHERE accueilId = 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageFormulaire`(in inImageFormulairePath varchar(500))
BEGIN
	update accueil
    SET         
        imageFormulairePath = inImageFormulairePath
        
	WHERE
     accueilId= 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageHomelie`(in inImageHomeliePath varchar(500))
BEGIN
	update accueil
    SET         
        imageHomeliePath = inImageHomeliePath
        
	WHERE
     accueilId= 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageMagasin`(in inImageMagasinId int(11), in inActif tinyint, in inOrderDisplay int)
BEGIN
	update imagemagasin
    SET 
        actif 		 = inActif,
        orderDisplay = inOrderDisplay
        
	WHERE
     imageMagasinId= inImageMagasinId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageNouvelle`(in inNouvellesId int(11), in inImagePath varchar(500))
BEGIN
	INSERT INTO `nouvelles`(title, descrSomm, descrTot, dateDebut, dateFin, actif, imagePath) SELECT 'Défault', '', '', unix_timestamp(curdate()), unix_timestamp(curdate()), 0, inImagePath WHERE inNouvellesId = 0;
	UPDATE `nouvelles`
    SET 
		imagePath = inImagePath
	WHERE
    nouvellesId = inNouvellesId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageTemoignage`(in inImageTemoignagePath varchar(500))
BEGIN
	update accueil
    SET         
        imageTemoignagePath = inImageTemoignagePath
        
	WHERE
     accueilId= 1;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateImageTemplateText`(IN inMenuId INT(11), IN inImagePath varchar(500))
BEGIN
	INSERT INTO pagecontenttemplatetext(menuid, image ) SELECT inMenuId,  inImagePath WHERE inMenuId not in (select menuId from pageContentTemplateText);
    
    UPDATE pagecontenttemplatetext SET image = inImagePath WHERE menuid = inMenuId ;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateNouvelles`(
	in inNouvelleId int(11), 
    in inTitle varchar(50),
	in inDescrSomm VARCHAR(100),
	in inDescrTot LONGTEXT,
	in inDateDebut INT(11),
	in inDateFin INT(11),
	in inActif tinyint)
BEGIN
	update `nouvelles`
    SET 
		title = inTitle,
		descrSomm = inDescrSomm,
		descrTot = inDescrTot,
		dateDebut = inDateDebut,
		dateFin = inDateFin,
		actif = inActif
		
	WHERE
    nouvellesId = inNouvelleId;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `UpdateVideoBienvenue`(in inVideoBienvenuePath varchar(500))
BEGIN
	update accueil
    SET         
        videoBienvenuePath = inVideoBienvenuePath
        
	WHERE
     accueilId= 1;
END ;;
DELIMITER ;
