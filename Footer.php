<?php
  require_once 'Class/clsAdmin.php';
?>
<footer class="footer">
	<script>
		if (window.top === window.self) {
			window.top.location.replace('Menu.php');
		}

	</script>
	<div>
		<div class="row" style="width:100%">

			<div class="col-12 col-lg-4 mb-5 mb-lg-0 d-flex flex-row align-items-center justify-content-center"> 
				<div class="">
					<img class="d-block img-fluid rounded" style="height:65px" src="../Ressource/Image/facebook.png" alt="Logo Facebook" title="Logo Facebook">
				</div>
				<div style="margin-left:10px;">
					<div d-flex flex-row justify-content-space-between style="margin-bottom: 2px">
						<img class="rounded" style="width:30px" src="../Ressource/Image/Logo_SGS.jpg" 
							alt="Logo Saint-Georges-de-Sartigan" title="Logo Saint-Georges-de-Sartigan">
						<a href="http://www.facebook.com/Paroisse-Saint-Georges-de-Sartigan-102528648168071/" target="_blank" style="color:white">Saint-Georges-de-Sartigan</a>
					</div>
					<div d-flex flex-row justify-content-space-between>
						<img class="rounded" style="width:30px" src="../Ressource/Image/Logo_St-Jean-Paul_II.jpg" 
							alt="Logo Saint-Jean-Paul-II" title="Logo Saint-Jean-Paul-II">
						<a href="http://www.facebook.com/paroisse.saintjeanpaulii.58" target="_blank" style="color:white">Saint-Jean-Paul II</a>
					</div>
				</div>
			</div>

			<div class="row col-12 col-lg-8">

				<div class="col-12 col-sm-4 mb-3">
					<div class="text-center">
						<input class="btn btn-primary" type="button" name="btnLiensInteressants" value="Liens intéressants" onclick="parent.fnRedirection('InformationPages/TemplateText.php', 100);">
					</div>
				</div>
				<div class="col-12 col-sm-4 mb-3">
					<div class="text-center">
						<input class="btn btn-primary" type="button" name="btnContact" value="Contactez-nous" onclick="parent.fnRedirection('Formulaire/FormulaireContact.php', <?php echo $_SESSION['gmenuId']; ?>);">
					</div>
				</div>
				<div class="col-12 col-sm-4 ">
					<div class="text-center">
						<?php 
							if(Admin::isConnected()){
								echo '<input class="btn btn-primary" type="button" name="btnDeconnexion" onclick="parent.fnDeconnexion()" value="Déconnexion">';
							}else{
								echo '<input  class="btn btn-primary" type="button" name="btnConnexion" onclick="parent.fnRedirection(\'Connexion/ConnexionAdmin.php\',0)" value="Connexion">';
							}
						?>
					</div>
				</div>

			</div>
		</div>

	</div>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71811-12"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-71811-12');
	</script>


</footer>
